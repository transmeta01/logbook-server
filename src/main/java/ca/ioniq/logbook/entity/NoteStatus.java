/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */


@Entity
@Table(name = "note_status")
public class NoteStatus extends BaseEntity {
  private static final long serialVersionUID = 1L;

  @Column(nullable = false)
  private String status;

  @Column(nullable = false)
  private String description;

  public NoteStatus() {
  }

  public NoteStatus(String status, String description) {
    this.status = status;
    this.description = description;
  }

  public String getStatus() {
    return status;
  }

  public NoteStatus setStatus(String status) {
    this.status = status;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public NoteStatus setDescription(String description) {
    this.description = description;
    return this;
  }
}

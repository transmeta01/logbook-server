/*
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.security.authentication;

import ca.ioniq.logbook.entity.Credential;
import com.google.inject.ImplementedBy;

import java.io.IOException;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@ImplementedBy(EncryptionServiceImpl.class)
public interface EncryptionService {

  String encryptPassword(String clearPassword, byte[] salt);

  Credential getCredentialFromClearPassword(String clearPassword);

  boolean verifyPassword(String plainPassword, Credential credential) throws IOException;
}


/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */
package ca.ioniq.logbook.dao;

import ca.ioniq.logbook.entity.*;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class AuditDao extends GenericDAO<Audit> {

  @Inject
  public AuditDao(Provider<EntityManager> em) {
    super(em, Audit.class);
  }

  public AuditDao(Class<Audit> persistentClass) {
    super(persistentClass);
  }

  public Audit saveAudit(Audit audit) {
    return super.persist(audit);
  }

  public List<Audit> findAuditByDateRange(Date start, Date end) {
    javax.persistence.TypedQuery query = provider.get().createQuery("SELECT a FROM Audit a WHERE a.lastModifiedDate BETWEEN :start AND :end", Audit.class)
      .setParameter("start", start, TemporalType.DATE)
      .setParameter("end", end, TemporalType.DATE);

    return query.getResultList();
  }

  public List<Audit> findAuditBySector(Sector sector) {
    TypedQuery<Audit> query = provider.get().createQuery("SELECT a FROM Audit a WHERE a.sector = :sector", Audit.class)
      .setParameter("sector", sector);

    return query.getResultList();
  }

  public List<Audit> findAuditByUser(User user) {
    TypedQuery<Audit> query = provider.get().createQuery("SELECT a FROM Audit a WHERE a.user = :user", Audit.class)
      .setParameter("user", user);

    return query.getResultList();
  }


  public List<Audit> findAuditByKeyword(String keyword) {
    TypedQuery<Audit> query = provider.get().createQuery("SELECT a FROM Audit a WHERE a.noteText LIKE '%:keyword%'", Audit.class)
      .setParameter("keyword", keyword);

    return query.getResultList();
  }

  public List<Audit> findAuditByPilotStatus(PilotStatus pilotStatus) {
    TypedQuery<Audit> query = provider.get().createQuery("SELECT a FROM Audit a WHERE a.pilotStatus = :pilotStatus", Audit.class)
      .setParameter("pilotStatus", pilotStatus);

    return query.getResultList();
  }

  public Audit findAuditForNote(Note note) {
    TypedQuery<Audit> query = provider.get().createQuery("SELECT a FROM Audit a WHERE a.note =:note order by lastModifiedDate desc", Audit.class)
      .setParameter("note", note);

    return query.getResultList().get(0);
  }

  public List<Audit> findAllAudit() {
    TypedQuery<Audit> query = provider.get().createQuery("SELECT a FROM Audit a", Audit.class);
    return query.getResultList();
  }
}


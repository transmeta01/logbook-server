/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.integration;

import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */
public class Slf4JTypeListener implements TypeListener {
  public <T> void hear(TypeLiteral<T> type, TypeEncounter<T> encounter) {
    for (Field field : type.getRawType().getDeclaredFields()) {
      if (field.getType() == Logger.class &&
        field.isAnnotationPresent(InjectLogger.class)) {

        encounter.register(new Slf4JMembersInjector<T>(field));
      }
    }
  }
}



package ca.ioniq.logbook.dao;

import ca.ioniq.logbook.entity.NoteStatus;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.List;

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class NoteStatusDao extends GenericDAO<NoteStatus> {

  @Inject
  public NoteStatusDao(Provider<EntityManager> em) {
    super(em, NoteStatus.class);
  }

  public NoteStatusDao(Provider<EntityManager> em, Class<NoteStatus> persistentClass) {
    super(em, persistentClass);
  }

  public List<NoteStatus> findAll() {
    return provider.get().createQuery("SELECT ns FROM NoteStatus ns", NoteStatus.class).getResultList();
  }

  public NoteStatus findNoteStatusByName(String notestatusName) {
    TypedQuery<NoteStatus> query = provider.get().createQuery("SELECT ns FROM NoteStatus ns WHERE description =:notestatusName", NoteStatus.class)
      .setParameter("notestatusName", notestatusName);

    return query.getSingleResult();
  }
}

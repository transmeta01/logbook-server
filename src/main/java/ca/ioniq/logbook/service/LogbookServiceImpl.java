package ca.ioniq.logbook.service;

import ca.ioniq.logbook.entity.*;
import ca.ioniq.logbook.exception.BadRequestException;
import ca.ioniq.logbook.service.admin.AdminService;
import ca.ioniq.logbook.service.audit.AuditService;
import ca.ioniq.logbook.service.note.NoteService;
import ca.ioniq.logbook.service.sector.SectorService;
import ca.ioniq.logbook.service.security.authorization.RequiresRole;
import ca.ioniq.logbook.service.user.UserService;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 *  <a href="http://www.apache.org/licenses/LICENSE-2.0">...</a>
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

// @todo i18n

@Slf4j
public class LogbookServiceImpl implements LogbookService {

  @Singleton
  private final AuditService auditService;
  @Singleton
  private final UserService userService;
  @Singleton
  private final AdminService adminService;
  @Singleton
  private final NoteService noteService;
  @Singleton
  private final SectorService sectorService;

  private List<User> _userCache;

  @Inject
  public LogbookServiceImpl(AuditService _auditService,
                            UserService _userService, AdminService _adminService,
                            NoteService _noteService, SectorService _sectorService) {

    auditService = _auditService;
    userService = _userService;
    adminService = _adminService;
    noteService = _noteService;
    sectorService = _sectorService;

    _userCache = userService.findAll();
  }

  public List<User> initCache() {
    _userCache = userService.findAll();
    return _userCache;
  }
  public Note saveOrUpdateNote(Note noteToSave, User user) {
    return noteService.saveOrUpdateNote(noteToSave, user);
  }

  public List<Note> findAllNotes() {
    return noteService.findNoteForToday();
  }

  public Note findNote(Long id) {
    return noteService.findNoteById(id);
  }

  public List<Note> findNoteForDate(Date date, Sector sector) {
    return noteService.findNoteListForDate(date, sector);
  }

  public List<Note> findNoteForToday() {
    return noteService.findNoteForToday();
  }

  public List<Note> findNoteByDateRange(Date startDate, Date endDate) {
    return noteService.findNoteByDateRange(startDate, endDate);
  }

  public List<Note> findNoteBySector(String sectorName) {
    Sector sector = sectorService.findSectorByName(sectorName);
    if (sector != null) {
      return findNoteBySector(sector.getId());
    } else {
      log.info("Sector with name : " + sectorName + " does not exists");
      throw new IllegalStateException("Le secteur avec le nom : " + sectorName + " n'existe pas");
    }
  }

  public List<Note> findNoteBySector(Sector sector) {
    return findNoteBySector(sector.getId());
  }

  public List<Note> findNoteBySector(Long sectorId) {
    Sector sector = sectorService.findSectorById(sectorId);
    return noteService.findNoteBySector(sector);
  }

  public List<Attachment> findNoteAttachments(Long noteId) {
    return noteService.findAttachmentsForNote(validateNote(noteId));
  }

  public Attachment findAttachments(Long attachementId) {
    return noteService.findAttachment(attachementId);
  }

  private Note validateNote(Long noteId) {
    Note result = noteService.findNoteById(noteId);
    if (result == null) {
      throw new BadRequestException("La note avec identifiant : " + noteId + "n'existe pas");
    }

    return result;
  }

  public Attachment addAttachment(Attachment attachment, Long noteId, Long userId) {
    return noteService.addAttachmentToNote(attachment, noteId, userId);
  }

  public Attachment addAttachment(Attachment attachment, Note note, Long userId) {
    return noteService.addAttachmentToNote(attachment, note.getId(), userId);
  }


  public boolean removeAttachment(Long attachmentId) {
    return Boolean.FALSE;
  }

  @RequiresRole(Role.ADMIN)
  public User saveOrUpdateUser(User userToSave, User user) {
    return userService.saveOrUpdate(userToSave);
  }

  public List<User> findAllUser(Long requestUserId) {
    return userService.findAll();
  }

  public User findUserById(Long userId) {
    return userService.findUserById(userId);
  }

  public User findUserByName(String username) {
    return userService.findUserByName(username);
  }

  public User findUserByLastName(String name) {
    return userService.findUserByLastName(name);
  }

  public Audit saveAudit(Audit auditToSave) {
    return auditService.saveAudit(auditToSave);
  }

  public List<Audit> findAuditBySector(Sector sector) {
    return auditService.findAuditBySector(sector);
  }

  public List<Audit> findAuditBySector(Long sectorId) {
    Sector sector = sectorService.findSectorById(sectorId);
    return auditService.findAuditBySector(sector);
  }

  public List<Audit> findAuditBySectorName(String sectorName) {
    Sector sector = sectorService.findSectorByName(sectorName);
    return auditService.findAuditBySector(sector);
  }

  public List<Audit> findAuditByDateRange(Date startDate, Date endDate) {
    return auditService.findAuditByDateRange(startDate, endDate);
  }

  public List<Audit> findAuditByPilotStatus(PilotStatus pilotStatus) {
    return auditService.findAuditByPilotStatus(pilotStatus);
  }

  public List<Audit> findAuditThatContainKeyword(String keyword) {
    return null;
  }

  @RequiresRole(Role.ADMIN)
  public PilotStatus saveOrUpdatePilotStatus(PilotStatus pilotStatus, Long userId) {
    return adminService.saveOrUpdatePilotStatus(pilotStatus, userId);
  }

  public List<PilotStatus> findAllPilotStatus() {
    return adminService.findAllPilotStatus();
  }

  public void deletePilotStatus(Long id) {
    adminService.deletePilotStatus(id);
  }

  public User authenticate(String username, String password) {
    return userService.authenticate(username, password);
  }

  @RequiresRole(Role.ADMIN)
  public boolean deleteUser(User user) {
    boolean deleted = false;

    try {
      userService.delete(user);
      deleted = true;
    } catch (Exception e) {
      log.error(e.getMessage());
      e.printStackTrace();
    }

    return deleted;
  }

  public List<Sector> findAllSector() {
    return sectorService.getAllSector();
  }

  public List<NoteStatus> findAllNoteStatus() {
    return noteService.findAllNoteStatus();
  }

  public PilotStatus findPilotStatusById(Long pilotStatusId) {
    return adminService.findPilotStatusById(pilotStatusId);
  }

  public NoteStatus findNoteStatusById(Long noteStatusId) {
    return noteService.findNoteStatusById(noteStatusId);
  }

  public Sector findSectorById(Long sectorId) {
    return sectorService.findSectorById(sectorId);
  }

  public Audit findAuditForNote(Note note) {
    return auditService.findAuditForNote(note);
  }

  public List<Audit> findAllAudit() {
    return auditService.findAllAudit();
  }

  public Sector findSectorByName(String sectorName) {
    return sectorService.findSectorByName(sectorName);
  }

  public List<Audit> findAuditByUser(User user) {
    return auditService.findAuditByUser(user);
  }

  public boolean uploadFile(InputStream uploadedInputStream, String dirPath, String fileName) {
    return noteService.uploadFile(uploadedInputStream, dirPath, fileName);
  }
}

package ca.ioniq.logbook.service.security.authentication;

import ca.ioniq.logbook.entity.Credential;

import java.io.IOException;
import java.security.SecureRandom;

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class EncryptionServiceImpl extends AbstractEncryptionService {
  private static final int SALT_LENGTH = 8;

  public EncryptionServiceImpl() {
  }

  public String encryptPassword(String clearPassword, byte[] salt) {
    saltGenerator.setSalt(salt);
    return passwordEncryptor.encryptPassword(clearPassword);
  }

  public Credential getCredentialFromClearPassword(String clearPassword) {
    byte[] seed = SecureRandom.getSeed(SALT_LENGTH);

    Credential credential = new Credential();
    credential.setSalt(java.util.Base64.getEncoder().encodeToString(seed));
    credential.setPassword(encryptPassword(clearPassword, seed));

    return credential;
  }

  public boolean verifyPassword(String plainPassword, Credential credential) throws IOException {
    saltGenerator.setSalt(java.util.Base64.getDecoder().decode(credential.getSalt()));

    return passwordEncryptor.checkPassword(plainPassword, credential.getPassword());
  }

}


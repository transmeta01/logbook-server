/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.dao;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

import ca.ioniq.logbook.entity.Attachment;
import ca.ioniq.logbook.entity.Note;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.List;

@Slf4j
public class AttachmentDao extends GenericDAO<Attachment> {

  @Inject
  public AttachmentDao(Provider<EntityManager> em) {
    super(em, Attachment.class);
  }

  public AttachmentDao(Class<Attachment> persistentClass) {
    super(persistentClass);
  }

  public List<Attachment> findAttachmentByNote(Note note) {
    TypedQuery<Attachment> query = provider.get().createQuery("SELECT a FROM Attachment a WHERE note = :note", Attachment.class)
      .setParameter("note", note);

    log.info(query.toString());
    return query.getResultList();
  }
}

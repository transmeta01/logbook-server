/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.sector;

import ca.ioniq.logbook.dao.SectorDao;
import ca.ioniq.logbook.entity.Sector;
import ca.ioniq.logbook.util.SectorEnum;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class SectorServiceImpl implements SectorService {
  private SectorDao sectorDao;

  private List<Sector> _sectorList;

  @Inject
  public SectorServiceImpl(SectorDao sectorDao) {
    this.sectorDao = sectorDao;
  }

  public Sector findSectorByName(String sectorname) {
    return sectorDao.findByName(sectorname);
  }

  public Sector findSectorById(Long id) {
    return sectorDao.findById(id);
  }

  public List<Sector> getAllSector() {
    _sectorList = sectorDao.findAll();

    if (_sectorList == null || _sectorList.isEmpty()) {
      initSector();
    }

    _sectorList = sectorDao.findAll();
    return _sectorList;
  }

  public void saveOrUpdate(Sector sector) {
    sectorDao.merge(sector);
  }

  public Sector save(Sector sector) {
    return sectorDao.persist(sector);
  }

  public void initSector() {
    List<Sector> sectorList = Arrays.asList(
      new Sector(SectorEnum.ESC_QBC.toString(), SectorEnum.ESC_QBC.toString()),
      new Sector(SectorEnum.QBC_TRV.toString(), SectorEnum.QBC_TRV.toString()),
      new Sector(SectorEnum.TRV_MTL.toString(), SectorEnum.TRV_MTL.toString()),
      new Sector(SectorEnum.PORT_MTL.toString(), SectorEnum.PORT_MTL.toString()),
      new Sector(SectorEnum.LOGBOOK_DISPATCH.toString(), SectorEnum.LOGBOOK_DISPATCH.toString()));

    for (Sector sector : sectorList) {
      save(sector);
    }
  }
}

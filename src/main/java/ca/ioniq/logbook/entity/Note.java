/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@Entity
@Table(name = "note")
public class Note extends BaseEntity {
  private static final long serialVersionUID = 1L;

  @Lob
  @Column(length = 300)
  private String noteText;

  @OneToOne
  @JoinColumn(nullable = false)
  private PilotStatus pilotStatus;

  @OneToOne
  @JoinColumn(nullable = false)
  private NoteStatus noteStatus;

  @OneToOne
  @JoinColumn(nullable = false)
  private Sector sector;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(nullable = false)
  private Date date;

  @Temporal(TemporalType.TIME)
  @Column(nullable = false)
  private Date time;

  @Column(nullable = false)
  private boolean active;

  public Note() {
  }

  public PilotStatus getPilotStatus() {
    return pilotStatus;
  }

  public Note setPilotStatus(PilotStatus pilotStatus) {
    this.pilotStatus = pilotStatus;
    return this;
  }

  public String getNoteText() {
    return noteText;
  }

  public Note setNoteText(String noteText) {
    this.noteText = noteText;
    return this;
  }

  public NoteStatus getNoteStatus() {
    return noteStatus;
  }

  public Note setNoteStatus(NoteStatus noteStatus) {
    this.noteStatus = noteStatus;
    return this;
  }

  public Sector getSector() {
    return sector;
  }

  public Note setSector(Sector sector) {
    this.sector = sector;
    return this;
  }

  public Date getDate() {
    return date;
  }

  public Note setDate(Date date) {
    this.date = date;
    return this;
  }

  public Date getTime() {
    return time;
  }

  public Note setTime(Date time) {
    this.time = time;
    return this;
  }

  public boolean isActive() {
    return active;
  }

  public Note setActive(boolean active) {
    this.active = active;
    return this;
  }
}


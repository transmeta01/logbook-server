/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.resource;

import ca.ioniq.logbook.entity.Sector;
import ca.ioniq.logbook.service.LogbookService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class SectorResourceImpl implements SectorResource {
  private final LogbookService logbookService;

  @Inject
  public SectorResourceImpl(LogbookService logbookService) {
    this.logbookService = logbookService;
  }

  public Response getAllSector() {
    List<Sector> sectorList = logbookService.findAllSector();
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    return Response.status(Response.Status.OK).entity(gson.toJson(sectorList)).build();
  }
}


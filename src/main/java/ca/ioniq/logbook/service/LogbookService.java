/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service;

import ca.ioniq.logbook.entity.*;
import com.google.inject.ImplementedBy;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@ImplementedBy(LogbookServiceImpl.class)
public interface LogbookService {

  Note saveOrUpdateNote(Note noteToSave, User user);

  Note findNote(Long id);

  List<Note> findAllNotes();

  List<Note> findNoteForDate(Date date, Sector sector);

  List<Note> findNoteForToday();

  List<Note> findNoteByDateRange(Date startDate, Date endDate);

  List<Note> findNoteBySector(String sector);

  List<Note> findNoteBySector(Sector sector);

  List<Note> findNoteBySector(Long sectorId);

  List<Attachment> findNoteAttachments(Long noteId);

  Attachment addAttachment(Attachment attachment, Long noteId, Long userId);

  Attachment addAttachment(Attachment attachment, Note note, Long userId);

  boolean removeAttachment(Long attachmentId);

  Attachment findAttachments(Long attachmentId);

  List<NoteStatus> findAllNoteStatus();

  NoteStatus findNoteStatusById(Long long1);

  // users
  User saveOrUpdateUser(User userToSave, User user);

  List<User> findAllUser(Long requestUserId);

  User findUserById(Long userId);

  User findUserByName(String username);

  User findUserByLastName(String name);

  User authenticate(String username, String password);

  boolean deleteUser(User user);

  Audit saveAudit(Audit auditToSave);

  List<Audit> findAuditBySector(Sector sector);

  List<Audit> findAuditBySector(Long sectorId);

  List<Audit> findAuditBySectorName(String sectorName);

  List<Audit> findAuditByDateRange(Date startDate, Date endDate);

  List<Audit> findAuditByUser(User user);

  List<Audit> findAuditByPilotStatus(PilotStatus pilotStatus);

  List<Audit> findAuditThatContainKeyword(String keyword);

  Audit findAuditForNote(Note note);

  List<Audit> findAllAudit();

  PilotStatus saveOrUpdatePilotStatus(PilotStatus pilotStatus, Long userId);

  PilotStatus findPilotStatusById(Long pilotStatusId);

  List<PilotStatus> findAllPilotStatus();

  void deletePilotStatus(Long id);

  // domain values
  List<Sector> findAllSector();

  Sector findSectorById(Long sectorId);

  Sector findSectorByName(String sectorName);

//  void initCache();

  boolean uploadFile(InputStream uploadedInputStream, String dirPath, String fileName);

  List<User> initCache();
}



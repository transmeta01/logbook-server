/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.ioniq.logbook.serialization;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * This class defines an exclusion strategy for Gson JSON parsing, i.e
 * types to ignore when converting entities to JSON format. This exclusion strategy
 * needed in order to avoid circular references which would invariably lead to an infinite
 * recursion and finally to a <code>StackOverFlowError</code>
 *
 * @author <a href="mailto:transmeta01@gmail.com">Richard Mutezintare</a>
 */
public class BaseGsonExclusionStrategy implements ExclusionStrategy {

  private final Class<?> _typeToSkip;

  public BaseGsonExclusionStrategy(Class<?> typeToSkip) {
    _typeToSkip = typeToSkip;
  }

  public boolean shouldSkipClass(Class<?> clazz) {
    return clazz == _typeToSkip;
  }

  public boolean shouldSkipField(FieldAttributes field) {
    return field.getAnnotation(Exclude.class) != null;
  }
}


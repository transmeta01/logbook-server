/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.audit;

import ca.ioniq.logbook.dao.AuditDao;
import ca.ioniq.logbook.entity.Audit;
import ca.ioniq.logbook.entity.Note;
import ca.ioniq.logbook.entity.User;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import jakarta.inject.Inject;
import jakarta.inject.Provider;
import java.util.Date;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class AuditMethodInterceptor implements MethodInterceptor {

  @Inject
  private Provider<AuditDao> _auditDaoProvider;

  public AuditMethodInterceptor() {
  }

  public Object invoke(MethodInvocation invocation) throws Throwable {
    Note modifiedNote = (Note) invocation.getArguments()[0];

//    Audit audit = new Audit();
//    audit.setCreationDate(new Date())
//      .setLastModifiedDate(new Date())
//      .setNoteDate(modifiedNote.getDate())
//      .setNote(modifiedNote)
//      .setNoteTime(modifiedNote.getTime())
//      .setNoteText(modifiedNote.getNoteText())
//      .setSector(modifiedNote.getSector());
//
//    User modifiedBy = (User) invocation.getArguments()[1];
//    audit.setModifiedBy(modifiedBy);
//    audit.setPilotStatus(modifiedNote.getPilotStatus());
//    audit.setNoteStatus(modifiedNote.getNoteStatus());
//
//    _auditDaoProvider.get().persist(audit);

    return invocation.proceed();
  }

  public void setAuditProvider(Provider<AuditDao> auditDaoProvider) {
    _auditDaoProvider = auditDaoProvider;
  }
}

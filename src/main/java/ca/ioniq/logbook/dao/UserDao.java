/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.dao;

import ca.ioniq.logbook.entity.Role;
import ca.ioniq.logbook.entity.User;
import javax.persistence.EntityManager;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class UserDao extends GenericDAO<User> {

  @Inject
  public UserDao(Provider<EntityManager> em) {
    super(em, User.class);
  }

  public UserDao(Class<User> persistentClass) {
    super(persistentClass);
  }

  public List<User> findUserByRole(Role role) {
    return provider.get().createQuery("SELECT u FROM User u WHERE u.role = :role", User.class)
      .setParameter("role", role)
      .getResultList();
  }

  public User findByUsername(String username) {
    return provider.get().createQuery("SELECT u FROM User u WHERE u.username = :username", User.class)
      .setParameter("username", username)
      .getSingleResult();
  }

  public List<User> findAll() {
    return provider.get().createQuery("SELECT u FROM User u", User.class).getResultList();
  }

}

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.resource;

import ca.ioniq.logbook.entity.*;
import ca.ioniq.logbook.exception.BadRequestException;
import ca.ioniq.logbook.serialization.NoteJsonParser;
import ca.ioniq.logbook.service.LogbookService;
import ca.ioniq.logbook.service.note.NoteServiceImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.core.header.FormDataContentDisposition;
import lombok.extern.slf4j.Slf4j;

import javax.activation.MimetypesFileTypeMap;
import javax.inject.Inject;
import javax.ws.rs.core.*;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@Slf4j
public class NoteResourceImpl implements NoteResource {

  private final LogbookService _logbookService;
  private final DateFormat _df = new SimpleDateFormat();
  private final DateFormat _dateFormat = new SimpleDateFormat("yyyy-MM-dd");
  private final NoteJsonParser _parser;

  @Context
  private UriInfo _uriInfo;

  @Inject
  public NoteResourceImpl(LogbookService logbookService, NoteJsonParser parser, UriInfo uriInfo) {
    _logbookService = logbookService;
    _parser = parser;
    _uriInfo = uriInfo;
  }

  public Response get(Long id) {
    Note result = _logbookService.findNote(id);

    String JSONResult = _parser.serialize(result);

    return Response.status(Response.Status.OK).entity(JSONResult).type(MediaType.APPLICATION_JSON).build();
  }

  public Response post(String json) {
    Note note = _parser.deserialize(json);

    User user = _parser.getUser();
    Note result = _logbookService.saveOrUpdateNote(note, user);

    UriBuilder ub = _uriInfo.getAbsolutePathBuilder();
    URI noteURI = ub.path(result.getId().toString()).build();

    String jsonResult = _parser.serialize(result);

    return Response.created(noteURI)
      .entity(jsonResult)
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response put(String json) {
    Note note = _parser.deserialize(json);

    User user = _parser.getUser();
    Note result = _logbookService.saveOrUpdateNote(note, user);

    UriBuilder uriBuilder = _uriInfo.getAbsolutePathBuilder();
    URI noteURI = uriBuilder.path(result.getId().toString()).build();

    return Response.created(noteURI).entity(_parser.serialize(result))
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response getNoteForDate(String date, Long sectorId) {
    Sector sector = _logbookService.findSectorById(sectorId);
    Date parsedDate = _parser.parseDate(date);
    List<Note> result = _logbookService.findNoteForDate(parsedDate, sector);

    String JSONResult = _parser.serialize(result);

    return Response.status(Response.Status.OK).entity(JSONResult)
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response getNoteBySector(Long id) {
    Sector sector = _logbookService.findSectorById(id);
    List<Note> result = _logbookService.findNoteBySector(sector);

    String JSONResult = _parser.serialize(result);

    return Response.status(Response.Status.OK).entity(JSONResult)
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response getNoteByDateRange(String start, String end) {
    Date startDate;
    Date endDate;
    List<Note> result;

    try {
      endDate = _df.parse(end);
      startDate = _df.parse(start);
      result = _logbookService.findNoteByDateRange(startDate, endDate);
    } catch (ParseException e) {
      log.error(e.getMessage());
      throw new BadRequestException(e.getMessage());
    }

    Gson gson = new Gson();

    return Response.status(Response.Status.OK).entity(gson.toJson(result))
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response getAllNoteStatus() {
    List<NoteStatus> result = _logbookService.findAllNoteStatus();
    Gson gson = new GsonBuilder()
      .setPrettyPrinting()
      .create();

    return Response.status(Response.Status.OK).entity(gson.toJson(result))
      .type(MediaType.APPLICATION_JSON)
      .build();
  }


  public Response getNoteAttachment(Long attachmentId) {
    Attachment result = _logbookService.findAttachments(attachmentId);

    File file = new File(result.getPath());

    String mimeType = new MimetypesFileTypeMap().getContentType(file);
    return Response.status(Response.Status.OK).entity(file)
      .header("content-disposition", "attachment; filename = " + result.getDocumentName())
      .header("Content-Length", file.length())
      .header("Content-type", mimeType)
      .type(mimeType)
      .build();
  }

  public Response postAttachment(InputStream uploadedInputStream, FormDataContentDisposition fileDetail, Long noteId, Long userId) {
    StringBuilder attachmentPath = new StringBuilder();

    Note currentNote = _logbookService.findNote(noteId);
    String date = _dateFormat.format(currentNote.getDate());
    String sector = currentNote.getSector().getDescription();

    attachmentPath.append(NoteServiceImpl.UPLOAD_FILE_DIR).append(sector).append("/").append(date).append("/");

    boolean success = _logbookService.uploadFile(uploadedInputStream, attachmentPath.toString(), fileDetail.getFileName());

    if (success) {
      Note resultNote = attachToNote(attachmentPath.append(fileDetail.getFileName()).toString(), noteId, userId);

      String jsonResult = _parser.serialize(resultNote);

      return Response.status(Response.Status.OK).entity(jsonResult).build();
    } else {
      return Response.status(Response.Status.BAD_REQUEST).build();
    }
  }

  private Note attachToNote(String attachmentPath, Long noteId, Long userId) {
    Attachment attachment = new Attachment();

    String[] pathNames = attachmentPath.split("/");
    String fileName = pathNames[pathNames.length - 1];

    attachment.setDocumentName(fileName)
      .setPath(attachmentPath);

    // could match more than 1 token,
    // make sure we only take the last token
    String[] tokens = fileName.split("\\.");
    String extension = tokens[tokens.length - 1];

    attachment.setDocumentType(extension);

    Note note = _logbookService.findNote(noteId);
    attachment.setNote(note);

    User user = _logbookService.findUserById(userId);

    attachment = _logbookService.addAttachment(attachment, note.getId(), user.getId());

    return _logbookService.saveOrUpdateNote(note, user);

  }
}


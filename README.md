# README #

This is a demonstration of a lightweight, RESTful web server to manage notes. It demonstrates the integration of
google-guice, JAX-RS (1), JPA 2.0, REST and service oriented architecture. It is self contained and driven by maven. See
the pom.xml file for all dependencies and plugins. This project is apache
2.0 (http://www.apache.org/licenses/LICENSE-2.0)

### How do I get set up? ###

* set-up maven and import the project into your favorite editor

* In the persistence.xml, there are 2 persistence units defined. One for hsqldb for in memory testing and another for
  Mysql for production. You may define another persistence unit for your DB of choice. To select the currently used
  persistence unit, give the name of the desired persistence unit to the JpaPersistModule in the LogbookConfiguration
  class.

* To run, cd into the root dir and issue : mvn jetty:run to start the server. Note, once started the server will listen
  for code changes and hot re-deploy with the changes. So no need to restart the server in between code modifications.

* To view all possible method calls and parameters type, request : http://localhost:8080/rest/application.wadl

### Who do I talk to? ###

* transmeta01@gmail.com

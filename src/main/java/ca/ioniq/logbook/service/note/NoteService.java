/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.note;

import ca.ioniq.logbook.entity.*;
import ca.ioniq.logbook.exception.NullEntity;
import com.google.inject.ImplementedBy;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@ImplementedBy(NoteServiceImpl.class)
public interface NoteService {

  @NullEntity(name = "note")
  Note findNoteById(Long noteId);

  List<Note> findNoteListForDate(Date date, Sector sector);

  @NullEntity(name = "secteur")
  List<Note> findNoteBySector(Sector sector);

  List<Note> findNoteByModificationDate(Date date);

  List<Note> findNoteByDateRange(Date startDate, Date endDate);

  @NullEntity(name = "utilisateur")
  List<Note> findNoteByUserName(User user);

  Note saveOrUpdateNote(Note newNote, User user);

  @NullEntity(name = "note")
  Attachment addAttachmentToNote(Attachment attachementToAdd, Long noteId, Long userId);

  @NullEntity(name = "attachment")
  void removeAttachment(Long attachementId, Long noteId);

  @NullEntity(name = "note")
  List<Attachment> findAttachmentsForNote(Note note);

  @NullEntity(name = "attachment")
  Attachment findAttachment(Long attachementId);

  List<NoteStatus> findAllNoteStatus();

  void saveNoteStatus(NoteStatus noteStatus);

  NoteStatus findNoteStatusById(Long noteStatusId);

  List<Note> findNoteForToday();

  boolean uploadFile(InputStream uploadedInputStream, String dirPath, String fileName);

}


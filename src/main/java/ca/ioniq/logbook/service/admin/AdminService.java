/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.admin;

import ca.ioniq.logbook.entity.PilotStatus;
import ca.ioniq.logbook.entity.User;
import com.google.inject.ImplementedBy;

import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@ImplementedBy(AdminServiceImpl.class)
public interface AdminService {

  User saveOrUpdateUser(User user, Long userId);

  List<User> findAllUser();

  PilotStatus saveOrUpdatePilotStatus(PilotStatus pilotStatus, Long userId);

  List<PilotStatus> findAllPilotStatus();

  void deletePilotStatus(Long id);

  PilotStatus findPilotStatusById(Long pilotStatusId);
}


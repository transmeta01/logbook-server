/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.util;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public enum MSWORD_EXTENSION {
  DOC {
    public String toString() {
      return "doc";
    }
  },

  DOCX {
    public String toString() {
      return "docx";
    }
  },

  DOCM {
    public String toString() {
      return "docm";
    }
  },

  DOTX {
    public String toString() {
      return "dotx";
    }
  },

  DOTM {
    public String toString() {
      return "dotm";
    }
  };

  public static MSWORD_EXTENSION find(String name) {
    for (MSWORD_EXTENSION extension : MSWORD_EXTENSION.values()) {
      if (extension.toString().equals(name)) {
        return extension;
      }
    }

    return null;
  }
}

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.resource;

import com.google.inject.ImplementedBy;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@ImplementedBy(NoteResourceImpl.class)
@Path("/note")
public interface NoteResource {

  @GET
  @Path("/sector/{id}")
  @Produces({MediaType.APPLICATION_JSON})
  Response getNoteBySector(@PathParam("id") Long id);

  @GET
  @Path("/{date}/{sectorid}")
  @Produces({MediaType.APPLICATION_JSON})
  Response getNoteForDate(@PathParam("date") String date, @PathParam("sectorid") Long sectorid);

  @GET
  @Path("/date/{start}/{end}")
  @Produces({MediaType.APPLICATION_JSON})
  Response getNoteByDateRange(@PathParam("start") String start, @PathParam("end") String end);

  @GET
  @Path("/{id}")
  @Produces({MediaType.APPLICATION_JSON})
  Response get(@PathParam("id") Long noteId);

  @POST
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_JSON})
  Response post(String json);

  @PUT
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.APPLICATION_JSON})
  Response put(String json);

  @GET
  @Path("/notestatus")
  @Produces({MediaType.APPLICATION_JSON})
  Response getAllNoteStatus();

  @GET
  @Path("/attachment/{id}")
  @Produces({MediaType.APPLICATION_OCTET_STREAM, "application/pdf", "application/doc", "application/xls"})
  Response getNoteAttachment(@PathParam("id") Long attachmentId);

  @POST
  @Path("/attachment/{noteid}/{userid}")
  @Produces({MediaType.APPLICATION_JSON})
  @Consumes({MediaType.MULTIPART_FORM_DATA})
  Response postAttachment(@FormDataParam("file") InputStream inputStream,
                          @FormDataParam("file") FormDataContentDisposition fileDetail,
                          @PathParam("noteid") Long noteId,
                          @PathParam("userid") Long userId);

}



/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.resource;

import ca.ioniq.logbook.entity.Credential;
import ca.ioniq.logbook.entity.User;
import ca.ioniq.logbook.serialization.BaseGsonExclusionStrategy;
import ca.ioniq.logbook.service.LogbookService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class UserResourceImpl implements UserResource {
  @Context
  UriInfo _uriInfo;
  private final LogbookService _logbookService;
  private final Gson _jsonParser;

  @Inject
  public UserResourceImpl(LogbookService logbookService, UriInfo uriInfo) {
    _logbookService = logbookService;
    _uriInfo = uriInfo;
    _jsonParser = new GsonBuilder()
      .setExclusionStrategies(new BaseGsonExclusionStrategy(Credential.class))
      .setPrettyPrinting()
      .create();
  }

  public Response authenticate(String username, String password) {
    User result = _logbookService.authenticate(username, password);

    return Response.status(Response.Status.OK)
      .entity(_jsonParser.toJson(result))
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response get(Long userId) {
    User result = _logbookService.findUserById(userId);

    return Response.status(Response.Status.OK)
      .entity(_jsonParser.toJson(result))
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response getAllUser(Long userId) {
    List<User> userList = _logbookService.findAllUser(userId);
    return Response.status(Response.Status.OK)
      .entity(_jsonParser.toJson(userList))
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response post(String json, Long userId) {
    User user = _jsonParser.fromJson(json, User.class);
    User requestingUser = _logbookService.findUserById(userId);

    User result = _logbookService.saveOrUpdateUser(user, requestingUser);

    UriBuilder ub = _uriInfo.getAbsolutePathBuilder();
    URI pilotStatusURI = ub.path(result.getId().toString()).build();

    return Response.created(pilotStatusURI)
      .entity(_jsonParser.toJson(result))
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response put(String json, Long userId) {
    User user = _jsonParser.fromJson(json, User.class);
    User requestingUser = _logbookService.findUserById(userId);
    user = _logbookService.saveOrUpdateUser(user, requestingUser);

    return Response.status(Response.Status.OK)
      .entity(_jsonParser.toJson(user))
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response delete(String json) {
    User user = _jsonParser.fromJson(json, User.class);
    _logbookService.deleteUser(user);
    return Response.status(Response.Status.OK).build();
  }
}


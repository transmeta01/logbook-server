package ca.ioniq.logbook.service.user;

import ca.ioniq.logbook.dao.UserDao;
import ca.ioniq.logbook.entity.Credential;
import ca.ioniq.logbook.entity.Role;
import ca.ioniq.logbook.entity.User;
import ca.ioniq.logbook.exception.NotAuthorizedException;
import ca.ioniq.logbook.service.security.authentication.AuthenticationService;
import ca.ioniq.logbook.service.security.authorization.RequiresRole;
import jakarta.validation.constraints.NotNull;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * <a href="http://www.apache.org/licenses/LICENSE-2.0">...</a>
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class UserServiceImpl implements UserService {

  private final UserDao userDao;
  private final AuthenticationService authenticationService;

  private final List<User> _userList = new ArrayList<User>();

  @Inject
  public UserServiceImpl(UserDao userDao, AuthenticationService authenticationService) {
    this.userDao = userDao;
    this.authenticationService = authenticationService;
  }

  public User findUserByName(@NotNull String username) {
    return userDao.findByUsername(username);
  }

  public User findUserByLastName(String name) {
    return userDao.findByUsername(name);
  }

  public User findUserById(@NotNull Long userId) {
    return userDao.findById(userId);
  }

  public List<User> findUserByRole(Role role) {
    return userDao.findUserByRole(role);
  }

  public User authenticate(String username, String password) {
    User userToAuthenticate = null;
    try {
      userToAuthenticate = findUserByName(username);
      if (userToAuthenticate != null) {
        boolean authenticated = authenticationService.authenticate(userToAuthenticate, password);
        if (!authenticated) {
          throw new NotAuthorizedException(
            "Le code utilisateur ou le mot de passe est Érroné");
        }
      }
    } catch (javax.persistence.NoResultException error) {
      throw new NotAuthorizedException("Le code utilisateur ou le mot de passe est \u00E9rron\u00E9");
    }

    return userToAuthenticate;
  }

  private List<User> initUsers() {
    List<User> userList = Arrays
      .asList(
        new User("SAUS", "Sauv\u00E9", "St\u00E9phane", Role.SUPERVISOR_DISPATCH.toString(), null, Boolean.TRUE),
        new User("JOUJ", "Jourdain", "Julie", Role.SUPERVISOR_DISPATCH.toString(), null, Boolean.TRUE),
        new User("MASS", "Masson", "Sylvia", Role.DIRECTOR.toString(), null, Boolean.TRUE),
        new User("BOLR", "Bolduc", "Richard", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("PAUJ", "Paulin", "Jerome", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("NOEJ", "Noel", "Jean Fran\u00E7ois", Role.SUPERVISOR_DISPATCH.toString(), null, Boolean.TRUE),
        new User("BOUC", "Boucher", "Christian", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("LAPS", "Lapointe", "Steve", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("BUHM", "Buhendwa", "Mukunguza", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("MORS", "Morin", "St\u00E9phane", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("SAUA", "Sauv\u00E9", "Alain", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("BLAG", "Blanchet", "Guildo", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("OUEM", "Oueslati", "Mohamed", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("DAIM", "Daigle", "Maxime", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("BUGC", "Bugeaud", "Caroline", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("NDIONN", "Dionne", "Nadia", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("NECHEV", "Echeverria", "Napoleon", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("MCOUTU", "Coutu", "Mhattieu", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("FBERGE", "Bergeron", "Francis", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("HCHICO", "Chicoine", "Hugo", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("IMIHAI", "Mihai", "Ion", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("CTHIBE", "Thibeault", "Chantal", Role.DISPATCHER.toString(), null, Boolean.TRUE),
        new User("SYSADMIN", "Syst\u00E8me", "Administrateur", Role.ADMIN.toString(), null, Boolean.TRUE)
      );


    // save list to database with password encrypted
    userList.forEach(user -> {
      Credential credential = authenticationService.getCredentialFromClearPassword(user.getFirstname());
      user.setCredential(credential);
      credential.setUser(user);

      save(user);
    });

    return userList;
  }

  public User save(User user) {
    return userDao.persist(user);
  }

  @RequiresRole(Role.ADMIN)
  public void delete(User user) {
    userDao.delete(user);
  }

  public User saveOrUpdate(User user) {
    return userDao.merge(user);
  }

  public List<User> findAll() {

    List<User> resultSet = userDao.findAll();
    return resultSet.isEmpty() ? this.initUsers() : resultSet;

  }

  public Credential getCredentialFromClearPassword(String password) {
    return authenticationService.getCredentialFromClearPassword(password);
  }
}


/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.dao;

import com.google.inject.persist.Transactional;

import javax.persistence.PersistenceContextType;
import javax.persistence.TypedQuery;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.inject.Provider;

/**
 * Base class for persistence
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public abstract class GenericDAO<T> implements DAO<T> {

  @PersistenceContext(type = PersistenceContextType.EXTENDED)
  Provider<EntityManager> provider;
  private final Class<T> persistentClass;

  public GenericDAO(Provider<EntityManager> provider, Class<T> persistentClass) {
    this.persistentClass = persistentClass;
    this.provider = provider;
  }

  public GenericDAO(Class<T> persistentClass) {
    this.persistentClass = persistentClass;
  }

  @Transactional
  public T persist(T t) {
    provider.get().persist(t);
    return t;
  }

  @Transactional
  public void delete(T t) {
    provider.get().remove(t);
  }

  @Transactional
  public T update(T t) {
    return provider.get().merge(t);
  }

  @Transactional
  public T merge(T t) {
    return provider.get().merge(t);
  }

  public T findById(Long id) {
    return provider.get().find(persistentClass, id);
  }
}

package ca.ioniq.logbook.resource;

import ca.ioniq.logbook.entity.PilotStatus;
import ca.ioniq.logbook.service.LogbookService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Inject;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.List;

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class PilotStatusResourceImpl implements PilotStatusResource {

  private final LogbookService _logbookService;
  private final Gson _jsonParser;

  @Context
  private UriInfo _uriInfo;

  @Inject
  public PilotStatusResourceImpl(LogbookService logbookService) {
    this._logbookService = logbookService;
    _jsonParser = new GsonBuilder().setPrettyPrinting().create();
  }

  public Response getAllPilotStatus() {
    List<PilotStatus> result = _logbookService.findAllPilotStatus();
    Gson gson = new GsonBuilder().create();

    return Response.status(Response.Status.OK)
      .type(MediaType.APPLICATION_JSON)
      .entity(gson.toJson(result))
      .build();
  }

  public Response post(String pilotStatus, Long userId) {
    PilotStatus deserialized = _jsonParser.fromJson(pilotStatus, PilotStatus.class);

    PilotStatus result = _logbookService.saveOrUpdatePilotStatus(deserialized, userId);

    UriBuilder ub = _uriInfo.getAbsolutePathBuilder();
    URI pilotStatusURI = ub.path(result.getId().toString()).build();

    Gson gson = new Gson();
    return Response.created(pilotStatusURI)
      .type(MediaType.APPLICATION_JSON)
      .entity(gson.toJson(result))
      .build();
  }

  public Response put(String pilotStatus, Long userId) {
    return post(pilotStatus, userId);
  }

  public Response delete(Long id) {
    _logbookService.deletePilotStatus(id);
    return Response.noContent().build();
  }
}

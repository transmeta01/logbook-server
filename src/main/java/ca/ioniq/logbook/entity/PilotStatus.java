/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@Entity
@Table(name = "pilot_status")
public class PilotStatus extends BaseEntity {
  private static final long serialVersionUID = 1L;

  @Column(nullable = false)
  private String status;

  @Column(nullable = false)
  private String description;

  @Column(nullable = false)
  private String statusCode;

  @Column(nullable = false)
  private boolean active;

  public PilotStatus() {
  }

  public PilotStatus(String _status, String _descritption, String _statusCode, boolean _active) {
    status = _status;
    description = _descritption;
    statusCode = _statusCode;
    active = _active;
  }


  public String getStatus() {
    return status;
  }

  public PilotStatus setStatus(String status) {
    this.status = status;
    return this;
  }

  public String getDescritption() {
    return description;
  }

  public PilotStatus setDescritption(String descritption) {
    this.description = descritption;
    return this;
  }

  public String getStatusCode() {
    return statusCode;
  }

  public PilotStatus setStatusCode(String statusCode) {
    this.statusCode = statusCode;
    return this;
  }

  public boolean isActive() {
    return active;
  }

  public PilotStatus setActive(boolean active) {
    this.active = active;
    return this;
  }
}


/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.resource;

import ca.ioniq.logbook.entity.Audit;
import ca.ioniq.logbook.entity.User;
import ca.ioniq.logbook.exception.BadRequestException;
import ca.ioniq.logbook.serialization.AuditJsonParser;
import ca.ioniq.logbook.service.LogbookService;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class AuditResourceImpl implements AuditResource {

  private Logger log = Logger.getLogger(this.getClass().getName());

  private LogbookService _logbookService;
  private AuditJsonParser _auditParser;

  private DateFormat _df = new SimpleDateFormat("dd MM yyyy");

  @Inject
  public AuditResourceImpl(LogbookService logbookService, AuditJsonParser auditParser) {
    _logbookService = logbookService;
    _auditParser = auditParser;
  }

  public Response getAuditByDateRange(String start, String end) {
    Date startDate;
    Date endDate;
    List<Audit> result;

    try {
      endDate = _df.parse(end);
      startDate = _df.parse(start);
      result = _logbookService.findAuditByDateRange(startDate, endDate);
    } catch (ParseException e) {
      log.severe(e.getMessage());
      throw new BadRequestException(e.getMessage());
    }

    String jsonResult = _auditParser.serialize(result);

    return Response.status(Response.Status.OK).entity(jsonResult).type(MediaType.APPLICATION_JSON).build();
  }


  public Response getAuditByUser(Long userId) {
    User user = _logbookService.findUserById(userId);

    List<Audit> result = _logbookService.findAuditByUser(user);

    String jsonResult = _auditParser.serialize(result);

    return Response.status(Response.Status.OK).entity(jsonResult).type(MediaType.APPLICATION_JSON).build();
  }

  public Response getAuditByKeyword(String keyword) {
    List<Audit> result = _logbookService.findAuditThatContainKeyword(keyword);

    return Response.status(Response.Status.OK)
      .entity(_auditParser.serialize(result))
      .type(MediaType.APPLICATION_JSON)
      .build();
  }

  public Response get() {
    List<Audit> result = _logbookService.findAllAudit();

    String jsonResult = _auditParser.serialize(result);

    return Response.status(Response.Status.OK).entity(jsonResult).type(MediaType.APPLICATION_JSON).build();
  }

  public Response getAuditBySectorName(String sectorname) {
    List<Audit> result = _logbookService.findAuditBySectorName(sectorname);

    String jsonResult = _auditParser.serialize(result);

    return Response.status(Response.Status.OK).entity(jsonResult).type(MediaType.APPLICATION_JSON).build();
  }
}


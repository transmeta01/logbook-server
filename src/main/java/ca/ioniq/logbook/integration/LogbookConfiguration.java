/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.integration;

import ca.ioniq.logbook.exception.EntityNotFoundInterceptor;
import ca.ioniq.logbook.exception.NullEntity;
import ca.ioniq.logbook.resource.*;
import ca.ioniq.logbook.service.security.authorization.AuthorizationService;
import ca.ioniq.logbook.service.security.authorization.RequiresRole;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;
import com.google.inject.matcher.Matchers;
import com.google.inject.persist.PersistFilter;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.GuiceServletContextListener;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */
public class LogbookConfiguration extends GuiceServletContextListener {

  protected Injector getInjector() {
    filter("/rest/*").through(PersistFilter.class);
    return null;
  }

//    protected void configureServlets() {
//
//    }

//    final Map<String, String> params = new HashMap<String, String>();
////		params.put("com.sun.jersey.config.feature.Trace", "true");
//
//    return Guice.createInjector(Stage.DEVELOPMENT,
//      new JerseyServletModule() {
//        @Override
//        protected void configureServlets() {
//
//          bind(UserResource.class);
//          bind(NoteResource.class);
//          bind(AuditResource.class);
//          bind(SectorResource.class);
//          bind(PilotStatusResource.class);
//
//          bindListener(Matchers.any(), new Slf4JTypeListener());
////						bindListener(Matchers.annotatedWith(Initialize.class), new LogbookServiceTypeListener());A
//
///*						AuditMethodInterceptor auditInterceptor = new AuditMethodInterceptor();
//						requestInjection(auditInterceptor);
//						bindInterceptor(Matchers.any(), Matchers.annotatedWith(AuditTrail.class), auditInterceptor);
//*/
//
//          AuthorizationService authorizationService = new AuthorizationService();
//          requestInjection(authorizationService);
//          bindInterceptor(Matchers.any(), Matchers.annotatedWith(RequiresRole.class), authorizationService);
//
//          EntityNotFoundInterceptor entityNotFound = new EntityNotFoundInterceptor();
//          bindInterceptor(Matchers.any(), Matchers.annotatedWith(NullEntity.class), entityNotFound);
//
//          filter("/rest/*").through(PersistFilter.class);
//
//          // init rest service
//          serve("/rest/*").with(GuiceContainer.class, params);
//          binder().bind(GuiceContainer.class).asEagerSingleton();
//
//        }
//
//      }, new JpaPersistModule("logbook-hsqldb-persistence"));
//  }
}

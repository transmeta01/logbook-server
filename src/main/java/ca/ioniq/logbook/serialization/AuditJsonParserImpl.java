package ca.ioniq.logbook.serialization;

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

import ca.ioniq.logbook.entity.Audit;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Custom JSON parser for audit
 */
public class AuditJsonParserImpl implements AuditJsonParser {

  private SimpleDateFormat dateFormatIn = new SimpleDateFormat("yyyy-MM-dd");
  private SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd-MM-yyyy");

  public AuditJsonParserImpl() {
  }

  public String serialize(Audit audit) {
    StringBuilder builder = new StringBuilder();
    builder.append("{");

    try {

      Date dateIn = dateFormatIn.parse(audit.getNoteDate().toString());
      String outDate = dateFormatOut.format(dateIn);

      Calendar cal = GregorianCalendar.getInstance();
      cal.setTime(audit.getNoteTime());
      int hour = cal.get(Calendar.HOUR_OF_DAY);
      int min = cal.get(Calendar.MINUTE);

      StringBuffer hourMin = new StringBuffer().append(hour).append(":");
      if (min == 0) {
        hourMin.append(min).append("0");
      } else if (min < 10) {
        hourMin.append("0").append(min);
      } else {
        hourMin.append(min);
      }

      builder.append("\"id\":").append(audit.getId()).append(",")
        .append("\"note_status\":").append("\"").append(audit.getNoteStatus().getStatus()).append("\"").append(",")
        .append("\"pilot_status\":").append("\"").append(audit.getPilotStatus().getStatus()).append("\"").append(",")
        .append("\"noteText\":").append("\"").append(audit.getNoteText()).append("\"").append(",")
        .append("\"username\":").append("\"").append(audit.getModifiedBy().getUsername()).append("\"").append(",")
        .append("\"role\":").append("\"").append(audit.getModifiedBy().getRole()).append("\"").append(",")
        .append("\"date\":").append("\"").append(outDate).append("\"").append(",")
        .append("\"sector\":").append("\"").append(audit.getNote().getSector().getSectorName()).append("\"").append(",")
        .append("\"time\":").append("\"").append(hourMin).append("\"").append(",")
        .append("\"note_id\":").append(audit.getNote().getId()).append(",");

      builder.append("\"attachment\":");
      if (audit.getAttachment() != null) {
        builder.append("\"").append(audit.getAttachment().getDocumentName()).append("\"");
      } else {
        builder.append("\"").append("\"");
      }

    } catch (java.text.ParseException e) {
      e.printStackTrace();
    }

    builder.append("}");


    return builder.toString();
  }

  public String serialize(List<Audit> auditList) {
    StringBuilder builder = new StringBuilder();

    builder.append("[");

    int i = 0;
    for (Audit audit : auditList) {
      builder.append(serialize(audit));

      if (i != auditList.size() - 1) {
        builder.append(",");
      }

      i++;
    }

    builder.append("]");
    return builder.toString();
  }
}


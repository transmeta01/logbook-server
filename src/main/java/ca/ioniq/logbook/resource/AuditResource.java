/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ca.ioniq.logbook.resource;

import com.google.inject.ImplementedBy;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@ImplementedBy(AuditResourceImpl.class)
@Path("/audit")
public interface AuditResource {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  Response get();

  @GET
  @Path("/date/{start}/{end}")
  @Produces(MediaType.APPLICATION_JSON)
  Response getAuditByDateRange(@PathParam("start") String start, @PathParam("end") String end);

  @GET
  @Path("/sector/{sectorname}")
  @Produces(MediaType.APPLICATION_JSON)
  Response getAuditBySectorName(@PathParam("sectorname") String sectorname);

  @GET
  @Path("/user/{userId}")
  @Produces(MediaType.APPLICATION_JSON)
  Response getAuditByUser(@PathParam("userId") Long userId);

  // should pass the keywords in the body of the request
  @GET
  @Path("/keyword/{keywords}")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  Response getAuditByKeyword(@PathParam("keywords") String keyword);

}

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.dao;

import ca.ioniq.logbook.entity.Sector;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class SectorDao extends GenericDAO<Sector> {

  @Inject
  public SectorDao(Provider<EntityManager> em) {
    super(em, Sector.class);
  }

  public SectorDao(Provider<EntityManager> em, Class<Sector> persistentClass) {
    super(em, persistentClass);
  }

  public List<Sector> findAll() {
    TypedQuery<Sector> query = provider.get().createQuery("SELECT s FROM Sector s", Sector.class);

    return query.getResultList();
  }

  public Sector findByName(String sectorname) {
    return provider.get().createQuery("SELECT s FROM Sector s WHERE s.sectorname=:sectorname", Sector.class)
      .setParameter("sectorname", sectorname)
      .getSingleResult();
  }
}


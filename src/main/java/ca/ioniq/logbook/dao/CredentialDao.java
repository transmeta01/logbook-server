/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.dao;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

import ca.ioniq.logbook.entity.Credential;
import ca.ioniq.logbook.entity.User;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;


public class CredentialDao extends GenericDAO<Credential> {

  @Inject
  public CredentialDao(Provider<EntityManager> em) {
    super(em, Credential.class);
  }

  public CredentialDao(Provider<EntityManager> em, Class<Credential> persistentClass) {
    super(em, persistentClass);
  }

  public Credential findByUserId(User user) {
    return provider.get().createQuery("SELECT cred FROM Credential cred WHERE cred.user = :user", Credential.class)
      .setParameter("user", user)
      .getSingleResult();
  }
}

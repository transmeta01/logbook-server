/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.dao;

/**
 * Allows to create <code>EntityManager</code> in a custom fashion
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

//import jakarta.persistence.EntityManager;
//import jakarta.persistence.EntityManagerFactory;

//import jakarta.persistence.EntityManager;EntityManager?

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class EntityManagerProvider implements Provider<EntityManager> {

  private final EntityManager entityManager;
  private EntityManager _entityManager;

  @Inject
  public EntityManagerProvider(EntityManagerFactory em) {
    entityManager = em.createEntityManager();
    _entityManager = entityManager;
  }

  public EntityManager get() {
    return _entityManager;
  }
}

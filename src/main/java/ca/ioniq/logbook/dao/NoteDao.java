/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.dao;

import ca.ioniq.logbook.entity.Note;
import ca.ioniq.logbook.entity.Sector;
import ca.ioniq.logbook.entity.User;
import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class NoteDao extends GenericDAO<Note> {

  public Logger log = Logger.getLogger(this.getClass().getName());

  @Inject
  public NoteDao(@Singleton Provider<EntityManager> em) {
    super(em, Note.class);
  }

  public NoteDao(Class<Note> persistenceClass) {
    super(persistenceClass);
  }

  public Note getNoteById(Long noteId) {
    TypedQuery<Note> query = provider.get().createQuery("SELECT n FROM Note n WHERE n.id = :id", Note.class)
      .setParameter("id", noteId);

    return query.getSingleResult();
  }

  public List<Note> findNoteBySector(Sector sector) {
    TypedQuery<Note> query = provider.get().createQuery("SELECT n FROM Note n WHERE n.sector = :sector", Note.class)
      .setParameter("sector", sector);

    return query.getResultList();
  }

  public List<Note> findByDate(Date date) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.DATE, 1);

    log.info(cal.getTime().toString());

    TypedQuery<Note> query =
      provider.get().createQuery("SELECT n FROM Note n WHERE n.date BETWEEN :from AND :to", Note.class)
        .setParameter("from", date, TemporalType.DATE)
        .setParameter("to", cal.getTime(), TemporalType.DATE);

    log.info(query.toString());

    return query.getResultList();
  }

  public List<Note> findNoteForToday() {
    return findByDate(new Date());
  }

  public List<Note> findByModificationDate(Date date) {
    TypedQuery<Note> query =
      provider.get().createQuery("SELECT n FROM Note n JOIN Audit at ON at.note_id = n.id WHERE at.lastModifiedDate = :modificationDate", Note.class)
        .setParameter("modificationDate", date, TemporalType.TIMESTAMP);

    return query.getResultList();
  }

  public List<Note> findByUserName(User user) {
    TypedQuery<Note> query = provider.get().createQuery("SELECT n FROM Note n JOIN Audit audit ON note.id = audit.note_id  JOIN user as u ON audit.user_id = u.id  WHERE audit.user = :user", Note.class)
      .setParameter("user", user);

    return query.getResultList();
  }

  public List<Note> findNoteByDateRange(Date startDate, Date endDate) {
    TypedQuery<Note> query = provider.get().createQuery("SELECT n FROM Note n JOIN Audit at ON at.note_id = n.id WHERE at.lastModifiedDate BETWEEN :startDate AND :endDate", Note.class)
      .setParameter("startDate", startDate, TemporalType.DATE)
      .setParameter("endDate", endDate, TemporalType.DATE);

    return query.getResultList();
  }

  public List<Note> findNoteForDate(Date date, Sector sector) {
    TypedQuery<Note> query = provider.get().createQuery("SELECT n FROM Note n WHERE date = :date AND sector = :sector", Note.class)
      .setParameter("date", date)
      .setParameter("sector", sector);

    return query.getResultList();
  }
}


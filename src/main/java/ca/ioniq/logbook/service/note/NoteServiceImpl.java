/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.note;

import ca.ioniq.logbook.dao.AttachmentDao;
import ca.ioniq.logbook.dao.AuditDao;
import ca.ioniq.logbook.dao.NoteDao;
import ca.ioniq.logbook.dao.NoteStatusDao;
import ca.ioniq.logbook.entity.*;
import ca.ioniq.logbook.exception.BadRequestException;
import ca.ioniq.logbook.service.user.UserService;
import ca.ioniq.logbook.util.NoteStatusEnum;
import com.google.inject.persist.Transactional;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import java.io.*;
import java.util.*;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@Slf4j
public class NoteServiceImpl implements NoteService {

  public static final String UPLOAD_FILE_DIR = "e:/attachments/";
  private final NoteDao noteDao;
  private final AttachmentDao attachementDao;
  private final NoteStatusDao noteStatusDao;
  private final  UserService userService;
  private final AuditDao auditDao;
  private final Map<Long, NoteStatus> noteStatusMap = new HashMap<>();

  @Inject
  public NoteServiceImpl(NoteDao _noteDao, AttachmentDao _attachmentDao, NoteStatusDao _noteStatusDao, UserService _userService, AuditDao _auditDao) {
    noteDao = _noteDao;
    attachementDao = _attachmentDao;
    noteStatusDao = _noteStatusDao;
    userService = _userService;
    auditDao = _auditDao;
  }

  public List<Note> findNoteListForDate(Date date, Sector sector) {
    return noteDao.findNoteForDate(date, sector);
  }

  public List<Note> findNoteForToday() {
    return noteDao.findNoteForToday();
  }

  public Note findNoteById(Long noteId) {
    return noteDao.findById(noteId);
  }

  public List<Note> findNoteBySector(Sector sector) {
    return noteDao.findNoteBySector(sector);
  }

  public List<Note> findNoteByModificationDate(Date date) {
    return noteDao.findByModificationDate(date);
  }

  public List<Note> findNoteByDateRange(Date startDate, Date endDate) {
    return noteDao.findNoteByDateRange(startDate, endDate);
  }

  public List<Note> findNoteByUserName(User user) {
    return noteDao.findByUserName(user);
  }

  @Transactional
  public Note saveOrUpdateNote(Note newNote, User modifiedBy) {

    newNote = noteDao.merge(newNote);

    Audit audit = new Audit();
    audit.setCreationDate(new Date())
      .setLastModifiedDate(new Date())
      .setNoteDate(newNote.getDate())
      .setNote(newNote)
      .setNoteTime(newNote.getTime())
      .setNoteText(newNote.getNoteText())
      .setSector(newNote.getSector());

    audit.setModifiedBy(modifiedBy);
    audit.setPilotStatus(newNote.getPilotStatus());
    audit.setNoteStatus(newNote.getNoteStatus());

    auditDao.saveAudit(audit);

    return newNote;
  }

  @Transactional
  public Attachment addAttachmentToNote(Attachment attachementToAdd, Long noteId, Long userId) {
    Note note = noteDao.getNoteById(noteId);
    attachementToAdd.setNote(note);

    attachementToAdd = attachementDao.persist(attachementToAdd);

    Audit audit = new Audit();
    audit.setCreationDate(new Date())
      .setLastModifiedDate(new Date())
      .setNoteDate(note.getDate())
      .setNote(note)
      .setNoteTime(note.getTime())
      .setNoteText(note.getNoteText())
      .setSector(note.getSector())
      .setPilotStatus(note.getPilotStatus())
      .setNoteStatus(note.getNoteStatus())
      .setAttachment(attachementToAdd);

    User modifiedBy = userService.findUserById(userId);
    audit.setModifiedBy(modifiedBy);

    auditDao.saveAudit(audit);

    return attachementToAdd;
  }

  public void removeAttachment(Long attachementId, Long noteId) {
    Attachment attachement = attachementDao.findById(attachementId);
    attachementDao.delete(attachement);
  }

  public List<Attachment> findAttachmentsForNote(Note note) {
    Note queryNote = noteDao.findById(note.getId());
    if (queryNote == null) {
      throw new BadRequestException("la note n'existe pas");
    }

    return attachementDao.findAttachmentByNote(queryNote);
  }

  public Attachment findAttachment(Long attachementId) {
    return attachementDao.findById(attachementId);
  }

  public List<NoteStatus> findAllNoteStatus() {
    List<NoteStatus> _noteStatusList = noteStatusDao.findAll();

    if (_noteStatusList == null || _noteStatusList.isEmpty()) {
      initNoteStatus();
    }

    _noteStatusList = noteStatusDao.findAll();
    return _noteStatusList;
  }

  public void saveNoteStatus(NoteStatus noteStatus) {
    noteStatusDao.persist(noteStatus);
  }

  private void initNoteStatus() {
    List<NoteStatus> noteStatusList = Arrays.asList(
      new NoteStatus(NoteStatusEnum.TRAITER.toString(), NoteStatusEnum.TRAITER.toString()),
      new NoteStatus(NoteStatusEnum.ATTENTE.toString(), NoteStatusEnum.ATTENTE.toString()),
      new NoteStatus(NoteStatusEnum.CANCELLER.toString(), NoteStatusEnum.CANCELLER.toString()),
      new NoteStatus(NoteStatusEnum.EN_RETARD.toString(), NoteStatusEnum.EN_RETARD.toString()));

    for (NoteStatus noteStatus : noteStatusList) {
      saveNoteStatus(noteStatus);
    }
  }

  public NoteStatus findNoteStatusById(Long noteStatusId) {
    NoteStatus noteStatus = noteStatusMap.get(noteStatusId);
    if (noteStatus == null) {
      noteStatus = noteStatusDao.findById(noteStatusId);
    }

    return noteStatus;
  }

  public boolean uploadFile(InputStream uploadedInputStream, String dirPath, String fileName) {
    boolean success = false;

    try {

      int read = 0;
      byte[] bytes = new byte[1024];
      File directory = null;
      File file = null;

      directory = new File(dirPath);
      if (!directory.exists())
        directory.mkdirs();

      directory.setWritable(true);
      directory.setReadable(true);

      file = new File(directory, fileName);

      OutputStream writer = new FileOutputStream(file);
      while ((read = uploadedInputStream.read(bytes)) != -1) {
        writer.write(bytes, 0, read);
      }
      writer.flush();
      writer.close();

      success = true;

    } catch (FileNotFoundException e) {
      log.error(e.getMessage());
      e.printStackTrace();
    } catch (IOException e) {
      log.error(e.getMessage());
      e.printStackTrace();
    }

    return success;
  }

  //supported document types
  public enum MIME {
    PDF {
      public String toString() {
        return "application/pdf";
      }
    },

    XLS {
      public String toString() {
        return "application/vnd.ms-excel";
      }
    },

    MSWORD {
      public String toString() {
        return "application/msword";
      }
    }
  }
}


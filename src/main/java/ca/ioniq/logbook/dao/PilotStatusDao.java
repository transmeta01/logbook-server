/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.dao;

import ca.ioniq.logbook.entity.PilotStatus;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class PilotStatusDao extends GenericDAO<PilotStatus> {

  @Inject
  public PilotStatusDao(Provider<EntityManager> em) {
    super(em, PilotStatus.class);
  }

  public PilotStatusDao(Provider<EntityManager> em, Class<PilotStatus> persistentClass) {
    super(em, persistentClass);
  }

  public PilotStatus saveOrUpdate(PilotStatus pilotStatus) {
    return provider.get().merge(pilotStatus);
  }

  public List<PilotStatus> findAll() {
    return provider.get().createQuery("SELECT ps FROM PilotStatus ps", PilotStatus.class).getResultList();
  }
}

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.security.authorization;

import ca.ioniq.logbook.dao.UserDao;
import ca.ioniq.logbook.entity.Role;
import ca.ioniq.logbook.entity.User;
import ca.ioniq.logbook.exception.NotAuthorizedException;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import javax.inject.Inject;
import javax.inject.Provider;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class AuthorizationService implements MethodInterceptor {

  @Inject
  private Provider<UserDao> _userDaoProvider;

  public AuthorizationService() {
  }

  public Object invoke(MethodInvocation invocation) throws Throwable {
    Role requiredRole = invocation.getMethod().getAnnotation(RequiresRole.class).value();
    Long userId = (Long) invocation.getArguments()[1];
    User user = _userDaoProvider.get().findById(userId);
    if (user == null || user.getRole() != requiredRole.toString()) {
      // @todo i18n
      throw new NotAuthorizedException("Vous n'avez pas les permissions nécessaires pour effectuer cet opération");
    }

    return invocation.proceed();
  }

  public void setProvider(Provider<UserDao> userDaoProvider) {
    _userDaoProvider = userDaoProvider;
  }
}

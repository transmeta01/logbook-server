/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.serialization;

import ca.ioniq.logbook.entity.*;
import ca.ioniq.logbook.service.LogbookService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class NoteJsonParserImpl implements NoteJsonParser {

  private static final Logger log = Logger.getLogger(NoteJsonParserImpl.class.getName());

  private LogbookService _logbookService;
  private User _user;

  private SimpleDateFormat requestDateFormat = new SimpleDateFormat("dd-MM-yyyy");
  private SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd MM yyyy");

  @Inject
  public NoteJsonParserImpl(LogbookService logbookService) {
    _logbookService = logbookService;
  }

  public Note deserialize(String json) throws JsonParseException {
    Note note = new Note();

    JsonParser parser = new JsonParser();
    JsonObject jsonOb = (JsonObject) parser.parse(json);

    // the user doing the request
    _user = _logbookService.findUserById(new Long(jsonOb.get("user_id").getAsLong()));

    Long id = !"".equals(jsonOb.get("id").getAsString()) ? new Long(jsonOb.get("id").getAsLong()) : null;
    note.setId(id);

    PilotStatus pilotStatus = _logbookService.findPilotStatusById(new Long(jsonOb.get("pilotStatus_id").getAsLong()));
    NoteStatus noteStatus = _logbookService.findNoteStatusById(new Long(jsonOb.get("noteStatus_id").getAsLong()));
    Sector sector = _logbookService.findSectorById(new Long(jsonOb.get("sector_id").getAsLong()));

    Date noteDate = null;
    String jsonDate = jsonOb.get("date").getAsString();
    try {
      noteDate = dateFormatOut.parse(jsonDate);
    } catch (ParseException e) {
      log.severe(e.getMessage());
      e.printStackTrace();
    }

    String time = jsonOb.get("time").getAsString();
    String hour = time.split(":")[0];
    String min = time.split(":")[1];

    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.HOUR_OF_DAY, new Integer(hour));
    cal.set(Calendar.MINUTE, new Integer(min));

    note.setDate(noteDate)
      .setPilotStatus(pilotStatus)
      .setNoteStatus(noteStatus)
      .setTime(cal.getTime())
      .setSector(sector)
      .setNoteText(jsonOb.get("noteText").getAsString());

    return note;
  }


  public User getUser() {
    return _user;
  }

  /**
   * {
   * "id": "",
   * "sector_id": 1,
   * "noteText": "new note 1",
   * "pilotStatus_id": 1,
   * "noteStatus_id": 4,
   * "date": "Thu Nov 15 15:46:54 EST 2012",
   * "time": "15:46",
   * "username": "JOUJ",
   * "user_id": 2
   * }
   *
   * @throws ParseException
   */
  public String serialize(Note note) {
    StringBuilder builder = new StringBuilder();
    builder.append("{");
    if (note != null) {

      Audit noteAudit = _logbookService.findAuditForNote(note);

      Calendar cal = GregorianCalendar.getInstance();
      cal.setTime(note.getTime());
      int hour = cal.get(Calendar.HOUR_OF_DAY);
      int min = cal.get(Calendar.MINUTE);

      StringBuffer hourMin = new StringBuffer().append(hour).append(":");
      if (min == 0) {
        hourMin.append(min).append("0");
      } else if (min < 10) {
        hourMin.append("0").append(min);
      } else {
        hourMin.append(min);
      }

      builder.append("\"id\":").append(note.getId()).append(",")
        .append("\"sector_id\":").append(note.getSector().getId()).append(",")
        .append("\"pilotStatus_id\":").append(note.getPilotStatus().getId()).append(",")
        .append("\"noteStatus_id\":").append(note.getNoteStatus().getId()).append(",")
        .append("\"noteText\":").append("\"").append(note.getNoteText()).append("\"").append(",")
        .append("\"username\":").append("\"").append(noteAudit.getModifiedBy().getUsername()).append("\"").append(",")
        .append("\"user_id\":").append(noteAudit.getModifiedBy().getId()).append(",")
        .append("\"noteDate\":").append("\"").append(note.getDate()).append("\"").append(",")
        .append("\"lastModified\":").append("\"").append(noteAudit.getLastModifiedDate().toString()).append("\"").append(",")
        .append("\"noteTime\":").append("\"").append(hourMin.toString()).append("\"");

      List<Attachment> attachments = _logbookService.findNoteAttachments(note.getId());

      builder.append(", \"attachments\":[");

      if (attachments != null && !attachments.isEmpty()) {

        int i = 0;
        for (Attachment attachment : attachments) {
          builder.append("{")
            .append("\"id\":").append(attachment.getId()).append(",")
            .append("\"name\":").append("\"").append(attachment.getDocumentName()).append("\"").append(",")
            .append("\"type\":").append("\"").append(attachment.getDocumentType()).append("\"").append(",")
            .append("\"path\":").append("\"").append(attachment.getPath()).append("\"")
            .append("}");

          if (i != attachments.size() - 1) {
            builder.append(",");
          }

          i++;
        }
      }

      builder.append("]");
    }

    builder.append("}");
    log.info(builder.toString());

    return builder.toString();
  }

  public String serialize(List<Note> noteList) {
    StringBuilder builder = new StringBuilder();

    builder.append("[");

    int i = 0;
    for (Note note : noteList) {
      builder.append(serialize(note));

      if (i != noteList.size() - 1) {
        builder.append(",");
      }

      i++;
    }

    builder.append("]");
    return builder.toString();
  }

  public Date parseDate(String date) {
    Date parsedDate = null;
    try {
      parsedDate = requestDateFormat.parse(date);
    } catch (ParseException e) {
      log.severe(e.getMessage());
      e.printStackTrace();
    }

    return parsedDate;
  }
}

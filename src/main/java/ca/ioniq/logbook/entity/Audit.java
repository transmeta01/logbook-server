package ca.ioniq.logbook.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * <a href="http://www.apache.org/licenses/LICENSE-2.0">...</a>
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */


@Entity
@Table(name = "audit_trail")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor(force = true)
public class Audit extends BaseEntity {
  @Temporal(TemporalType.TIMESTAMP)
  @Column(nullable = false)
  private final Date creationDate;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(nullable = false)
  private Date lastModifiedDate;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id", updatable = false, nullable = false)
  private User modifiedBy;

  @ManyToOne
  @JoinColumn(name = "note_id", updatable = false, nullable = false)
  private Note note;

  @ManyToOne
  @JoinColumn(name = "attachment_id", updatable = false)
  private Attachment attachment;

  @OneToOne
  @JoinColumn(name = "pilotStatus_id", nullable = false)
  private PilotStatus pilotStatus;

  @Lob
  @Column(length = 300, nullable = false)
  private String noteText;

  @OneToOne
  @JoinColumn(name = "noteStatus_id", nullable = false)
  private NoteStatus noteStatus;

  @OneToOne
  @JoinColumn(name = "sector_id", nullable = false)
  private Sector sector;

  @Temporal(TemporalType.DATE)
  @Column(nullable = false)
  private Date noteDate;

  @Temporal(TemporalType.TIME)
  @Column(nullable = false)
  private Date noteTime;

//  public Audit() {
//  }
//
//  public Date getCreationDate() {
//    return creationDate;
//  }
//
//  public Audit setCreationDate(Date creationDate) {
//    this.creationDate = creationDate;
//    return this;
//  }
//
//  public Date getLastModifiedDate() {
//    return lastModifiedDate;
//  }
//
//  public Audit setLastModifiedDate(Date lastModifiedDate) {
//    this.lastModifiedDate = lastModifiedDate;
//    return this;
//  }
//
//  public User getModifiedBy() {
//    return modifiedBy;
//  }
//
//  public Audit setModifiedBy(User modifiedBy) {
//    this.modifiedBy = modifiedBy;
//    return this;
//  }
//
//  public PilotStatus getPilotStatus() {
//    return pilotStatus;
//  }
//
//  public Audit setPilotStatus(PilotStatus pilotStatus) {
//    this.pilotStatus = pilotStatus;
//    return this;
//  }
//
//  public String getNoteText() {
//    return noteText;
//  }
//
//  public Audit setNoteText(String noteText) {
//    this.noteText = noteText;
//    return this;
//  }
//
//  public NoteStatus getNoteStatus() {
//    return noteStatus;
//  }
//
//  public Audit setNoteStatus(NoteStatus noteStatus) {
//    this.noteStatus = noteStatus;
//    return this;
//  }
//
//  public Sector getSector() {
//    return sector;
//  }
//
//  public Audit setSector(Sector sector) {
//    this.sector = sector;
//    return this;
//  }
//
//  public Date getNoteDate() {
//    return noteDate;
//  }
//
//  public Audit setNoteDate(Date date) {
//    this.noteDate = date;
//    return this;
//  }
//
//  public Date getNoteTime() {
//    return noteTime;
//  }
//
//  public Audit setNoteTime(Date time) {
//    this.noteTime = time;
//    return this;
//  }
//
//  public Note getNote() {
//    return note;
//  }
//
//  public Audit setNote(Note note) {
//    this.note = note;
//    return this;
//  }
//
//  public Attachment getAttachment() {
//    return attachment;
//  }
//
//  public void setAttachment(Attachment attachment) {
//    this.attachment = attachment;
//  }
}

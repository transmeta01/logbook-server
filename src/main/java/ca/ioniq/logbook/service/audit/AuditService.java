/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.audit;

import ca.ioniq.logbook.entity.*;
import com.google.inject.ImplementedBy;

import java.util.Date;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@ImplementedBy(AuditServiceImpl.class)
public interface AuditService {
  List<Audit> findAuditBySector(Sector sector);

  List<Audit> findAuditByDateRange(Date start, Date end);

  List<Audit> findAuditByUser(User user);

  List<Audit> findAuditByPilotStatus(PilotStatus pilotStatus);

  List<Audit> findAuditByKeyword(String keyword);

  List<Audit> findAuditByKeyword(String... keywords);

  Audit saveAudit(Audit auditToSave);

  Audit findAuditForNote(Note note);

  List<Audit> findAllAudit();
}

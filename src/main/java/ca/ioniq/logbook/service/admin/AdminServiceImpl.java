package ca.ioniq.logbook.service.admin;

import ca.ioniq.logbook.dao.PilotStatusDao;
import ca.ioniq.logbook.entity.PilotStatus;
import ca.ioniq.logbook.entity.Role;
import ca.ioniq.logbook.entity.User;
import ca.ioniq.logbook.service.security.authorization.RequiresRole;
import ca.ioniq.logbook.service.user.UserService;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class AdminServiceImpl implements AdminService {

  private PilotStatusDao pilotStatusDao;
  private UserService userService;

  private List<PilotStatus> _pilotStatusList;

  @Inject
  public AdminServiceImpl(PilotStatusDao pilotStatusDao, UserService userService) {
    this.pilotStatusDao = pilotStatusDao;
    this.userService = userService;
  }

  @RequiresRole(Role.ADMIN)
  public User saveOrUpdateUser(User user, Long userId) {
    return userService.saveOrUpdate(user);
  }

  public List<User> findAllUser() {
    return userService.findAll();
  }

  public PilotStatus saveOrUpdatePilotStatus(PilotStatus pilotStatus, Long userId) {
    return pilotStatusDao.merge(pilotStatus);
  }

  public List<PilotStatus> findAllPilotStatus() {
    _pilotStatusList = pilotStatusDao.findAll();

    if (_pilotStatusList == null || _pilotStatusList.isEmpty()) {
      initPilotStatusList();
    }

    _pilotStatusList = pilotStatusDao.findAll();
    return _pilotStatusList;
  }

  @RequiresRole(Role.ADMIN)
  public void deletePilotStatus(Long id) {
    PilotStatus toDelete = pilotStatusDao.findById(id);
    pilotStatusDao.delete(toDelete);
  }

  public PilotStatus findPilotStatusById(Long pilotStatusId) {
    return pilotStatusDao.findById(pilotStatusId);
  }

  // @todo i18n this
  private void initPilotStatusList() {
    List<PilotStatus> pilotStatusList = Arrays
      .asList(new PilotStatus("Malade 24 heures", "Malade 24 heures", "MAL", Boolean.TRUE),
        new PilotStatus("Malade longue dur\u00E9e", "Malade longue dur\u00E9e", "MALL", Boolean.TRUE),
        new PilotStatus("24 hres off", "24 hres off", "24HO", Boolean.TRUE),
        new PilotStatus("Absence", "Absence", "ABS", Boolean.TRUE),
        new PilotStatus("Affaire de cour", "Affaire de cour", "AFCO", Boolean.TRUE),
        new PilotStatus("Brevet suspendu", "Brevet suspendu", "BSUS", Boolean.TRUE),
        new PilotStatus("Comit\u00E9", "Comit\u00E9", "COM", Boolean.TRUE),
        new PilotStatus("Formation", "Formation", "FOR", Boolean.TRUE),
        new PilotStatus("Examen m\u00E9dical", "Examen m\u00E9dical", "EXM", Boolean.TRUE),
        new PilotStatus("Hospitalisation", "Hospitalisation", "HOS", Boolean.TRUE),
        new PilotStatus("Mortalit\u00E9", "Mortalit\u00E9", "MORT", Boolean.TRUE),
        new PilotStatus("Mortalit\u00E9(famille)", "Mortalit\u00E9(famille)", "MORTF", Boolean.TRUE),
        new PilotStatus("Non disponible a l'affectation", "Non disponible a l'affectation", "NDAFF", Boolean.TRUE),
        new PilotStatus("zD\u00E9c\u00E9d\u00E9", "zD\u00E9c\u00E9d\u00E9", "zDECE", Boolean.TRUE),
        new PilotStatus("Disponible au travail", "Disponible au travail", "DISP", Boolean.TRUE),
        new PilotStatus("Pr\u00E9-retaite", "Pr\u00E9-retaite", "PRET", Boolean.TRUE),
        new PilotStatus("Rappel (Port de Montreal)", "Rappel (Port de Montreal)", "HRAP", Boolean.TRUE),
        new PilotStatus("Pr\u00E9sident corporation", "Pr\u00E9sident corporation", "PRES", Boolean.TRUE),
        new PilotStatus("Lamaneur(Pilote Quebec-Escoumins)", "Lamaneur(Pilote Quebec-Escoumins)", "LAMA", Boolean.TRUE),
        new PilotStatus("Retraite", "Retraite", "RETR", Boolean.TRUE),
        new PilotStatus("Repos", "Repos", "REP", Boolean.TRUE),
        new PilotStatus("Vacances", "Vacances", "VAC", Boolean.TRUE),
        new PilotStatus("Dummy", "Dummy", "999", Boolean.TRUE),
        new PilotStatus("ICTA", "ICTA", "ICTA", Boolean.TRUE),
        new PilotStatus("ICTR", "ICTR", "ICTR", Boolean.TRUE),
        new PilotStatus("Prend son tour aux escoumins", "Prend son tour aux escoumins", "ESCM", Boolean.TRUE),
        new PilotStatus("PROFESSEUR", "PROFESSEUR", "PROF", Boolean.TRUE),
        new PilotStatus("ABSENT LONG TERME", "ABSENT LONG TERME", "ABS-LNG", Boolean.TRUE),
        new PilotStatus("Jour de banque", "Jour de banque", "BANK", Boolean.TRUE),
        new PilotStatus("Pr\u00E9-retraite (disp)", "Pr\u00E9-retraite (disp)", "PRETD", Boolean.TRUE),
        new PilotStatus("Extention de vacances CPBSL", "Extention de vacances CPBSL", "EXT-VAC", Boolean.TRUE),
        new PilotStatus("CONSULTANTS", "CONSULTANTS", "CONSUL", Boolean.TRUE),
        new PilotStatus("Paternit\u00E9", "Paternit\u00E9", "PAPA", Boolean.TRUE),
        new PilotStatus("Sans solde", "Sans solde", "SANS $", Boolean.TRUE));

    for (PilotStatus pilotStatus : pilotStatusList) {
      pilotStatusDao.persist(pilotStatus);
    }
  }
}


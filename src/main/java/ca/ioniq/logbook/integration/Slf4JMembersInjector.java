/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.integration;

import com.google.inject.MembersInjector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class Slf4JMembersInjector<T> implements MembersInjector<T> {
  private final Field field;
  private final Logger logger;

  Slf4JMembersInjector(Field field) {
    this.field = field;
    this.logger = LoggerFactory.getLogger(field.getDeclaringClass());
    field.setAccessible(Boolean.TRUE);
  }

  public void injectMembers(T instance) {
    try {
      field.set(instance, logger);
    } catch (IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }
}

/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.entity;

import ca.ioniq.logbook.serialization.Exclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@Entity
@Table(name = "user")
@AllArgsConstructor
@Getter
@Setter
public class User extends BaseEntity {
  @Column(nullable = false)
  private String username;

  @Column(nullable = false)
  private String lastname;

  @Column(nullable = false)
  private String firstname;

  @Column(nullable = false)
  private String role;

  @Exclude                                // exclude from serialisation
  @OneToOne(cascade = CascadeType.ALL)
  private Credential credential;

  @Column(nullable = false)
  private boolean active;

}


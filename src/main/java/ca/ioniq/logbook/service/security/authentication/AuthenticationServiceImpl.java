/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.security.authentication;

import ca.ioniq.logbook.dao.CredentialDao;
import ca.ioniq.logbook.entity.Credential;
import ca.ioniq.logbook.entity.User;

import javax.inject.Inject;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class AuthenticationServiceImpl implements AuthenticationService {
  Logger logger = Logger.getLogger(AuthenticationServiceImpl.class.getName());

  private CredentialDao credentialDao;
  private EncryptionService encryptionService;

  @Inject
  public AuthenticationServiceImpl(CredentialDao credentialDao, EncryptionService encryptionService) {
    this.credentialDao = credentialDao;
    this.encryptionService = encryptionService;
  }

  public Credential getCredentialFromClearPassword(String clearPassword) {
    return encryptionService.getCredentialFromClearPassword(clearPassword);
  }

  public boolean authenticate(Credential credential) {
    try {
      return encryptionService.verifyPassword("", credential);
    } catch (IOException e) {
      logger.severe(e.getMessage());
      e.printStackTrace();
    }

    return Boolean.FALSE;
  }

  public User authenticate(String username, String password) {
    return null;
  }

  public boolean authenticate(User user, String password) {
    Credential cred = credentialDao.findByUserId(user);
    if (cred != null) {
      try {
        return encryptionService.verifyPassword(password, cred);
      } catch (IOException e) {
        logger.severe(e.getMessage());
        e.printStackTrace();
      }
    }

    return Boolean.FALSE;
  }

}


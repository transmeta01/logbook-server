/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.Session;

/**
 * This class is is still somewhat iffy...could be useful...could be not! A experiment that turned
 * out to be useless in the scope of the current project.
 *
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@SuppressWarnings("unchecked")
public class GenericCriteria<T> {
  CriteriaBuilder criteriaBuilder;

  /**
   * Creates a new instance of GenericCriteria
   *
   * @param criteria Criteria to wrap
   */
  public GenericCriteria(CriteriaBuilder criteria) {
    this.criteriaBuilder = criteria;
  }

  public GenericCriteria(EntityManagerProvider entityManagerProvider, Class<T> clazz) {
    this.criteriaBuilder = entityManagerProvider.get().getCriteriaBuilder();

  }


//    /**
//     * Add a restriction to the criteria
//     * @param filter The criterion to apply
//     */
//    public void add(Criterion filter) {
//        criteria.add(filter);
//    }
//
//    /**
//     * Impose an ordering on the criterion query.
//     * @param order The order to impose
//     */
//    public void addOrder(Order order) {
//        criteria.addOrder(order);
//    }
//    /**
//     * Get the results
//     *
//     */
//    public java.util.List<T> list() throws HibernateException {
//        return criteria.list();
//    }
//
//    public T uniqueResult() {
//        return (T)criteria.uniqueResult();
//    }

//  public static CriteriaQuery create(Session session, Class targetClass) {
//    return session.getCriteriaBuilder().createQuery(targetClass);
////    return new GenericCriteria<E>(session, targetClass);
//  }
}

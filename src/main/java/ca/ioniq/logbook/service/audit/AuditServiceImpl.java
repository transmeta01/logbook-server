/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.audit;

import ca.ioniq.logbook.dao.AuditDao;
import ca.ioniq.logbook.entity.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

public class AuditServiceImpl implements AuditService {

  @Singleton
  private AuditDao _auditDao;

  @Inject
  public AuditServiceImpl(AuditDao auditDao) {
    _auditDao = auditDao;
  }

  public List<Audit> findAuditBySector(Sector sector) {
    return _auditDao.findAuditBySector(sector);
  }

  public List<Audit> findAuditByDateRange(Date start, Date end) {
    return _auditDao.findAuditByDateRange(start, end);
  }

  public List<Audit> findAuditByUser(User user) {
    return _auditDao.findAuditByUser(user);
  }

  public List<Audit> findAuditByPilotStatus(PilotStatus pilotStatus) {
    return _auditDao.findAuditByPilotStatus(pilotStatus);
  }

  public List<Audit> findAuditByKeyword(String keyword) {
    return null;
  }

  public List<Audit> findAuditByKeyword(String... keywords) {
    return null;
  }

  public Audit saveAudit(Audit auditToSave) {
    return _auditDao.persist(auditToSave);
  }

  public Audit findAuditForNote(Note note) {
    return _auditDao.findAuditForNote(note);
  }

  public List<Audit> findAllAudit() {
    return _auditDao.findAllAudit();
  }
}


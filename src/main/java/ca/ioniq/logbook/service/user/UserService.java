/**
 * Copyright 2012 logbook-server
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.ioniq.logbook.service.user;

import ca.ioniq.logbook.entity.Credential;
import ca.ioniq.logbook.entity.Role;
import ca.ioniq.logbook.entity.User;
import com.google.inject.ImplementedBy;

import java.util.List;

/**
 * @author <a href="transmeta01@gmail.com">Richard Mutezintare</a>
 */

@ImplementedBy(UserServiceImpl.class)
public interface UserService {

  User findUserByName(String userName);

  User findUserById(Long userId);

  List<User> findUserByRole(Role role);

  User authenticate(String username, String password);

  User save(User user);

  void delete(User user);

  User saveOrUpdate(User user);

  List<User> findAll();

  Credential getCredentialFromClearPassword(String firstName);

  User findUserByLastName(String name);
}

